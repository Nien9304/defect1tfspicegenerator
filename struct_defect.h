//
//	struct_defect.h
//	struct_defect.cpp
//
//	Files: .*defect
//	Structure:
//	-fault
//		type
//		-defect
//			name
//			locations
//			
//	Original project: Defect1tfSpiceGenerator
//	v1.0.0

#ifndef _STRUCT_DEFECT_H_
#define _STRUCT_DEFECT_H_
#include "common.h"

enum FAULT_TYPE{FT_BRIDGE, FT_OPEN, FT_TON, FT_TOFF, FT_DUAL, FT_INVALID};
static const char *FAULT_TYPE_NAME[] = 
	{"bridge", "open", "ton", "toff", "dual", "FT_INVALID"};
std::istream& operator >>(std::istream &is, FAULT_TYPE &type);

struct defect;
typedef vector<defect> UDT_DEFECT_VEC;
//defect
struct fault{
	void readSerialFile(ifstream &ifs);

	FAULT_TYPE m_type;
	UDT_DEFECT_VEC m_defect;
};
struct defect{
	string m_name;
	UDT_INT_VEC m_location;
};

#endif