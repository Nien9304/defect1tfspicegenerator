//
//	struct_pattern.h
//	struct_pattern.cpp
//
//	Files: .1tfpat .2tfpat
//	Structure:
//	-truthTable
//		input and output pin
//		1tf pattern table
//		2tf pattern table
//	
//	v1.0.1 //add write serial file function

#ifndef _STRUCT_PATTERN_H_
#define _STRUCT_PATTERN_H_
#include "common.h"

struct pattern{
	void readSerial1tfFile(ifstream &ifs);
	void readSerial2tfFile(ifstream &ifs);
	void writeSerial1tfFile(ostream &os);
	void writeSerial2tfFile(ostream &os);
	//the order of pins is related to the table's column
	//{inputs, outputs}
	UDT_STR_VEC m_inputPin;
	UDT_STR_VEC m_outputPin;
	//1tf 0/1
	UDT_STR_VEC m_truthTable;
	//2tf L/H/R/F
	UDT_STR_VEC m_transitionTable;
};

#endif