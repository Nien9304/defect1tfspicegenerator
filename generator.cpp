#include "struct.h"

void spiceGenerator::writeCombSpice(ostream &os){
	writeTitle(os);
	writeSpiceSetup(os);
	writePowerConnection(os);
	writeSimulationParameter(os);
	writeSimulationSetup(os);
	writeInputPattern(os);
	writeOutputMeasurement(os);
	writeNetlist(os);
	writeAlter(os);
	writeEnd(os);
}
void spiceGenerator::writeTitle(ostream &os){
	writeLine(os, "description");
	os << "* Usage: 1tf analysis" << endl;
	os << "* Cell: " << m_cell.m_name << endl;
	os << "* Type: " << CELL_TYPE_NAME[m_cell.m_type] << endl;
	os << "* Fault: " << m_faultName << endl;
	os << "* Value: " << m_valueName << endl;
}
void spiceGenerator::writeSpiceSetup(ostream &os){
	writeLine(os, "spice settings");
	os << m_lib;
	//option in config file
	if(m_option.size() != 0){
		os << ".option";
	}
	for(unsigned index = 0; index < m_option.size(); ++index){
		os  << " " << m_option[index];
	}
	os << endl;
	os << "*.option post" << endl;
	for(UDT_STR_VEC::iterator strIt = m_subckt.m_option.begin(); 
		strIt != m_subckt.m_option.end(); ++strIt){
		os << ".option" << *strIt << endl;
	}
	os << ".temp " << m_temp << endl;
	os << endl;
}
void spiceGenerator::writeSimulationParameter(ostream &os){
	writeLine(os, "parameter");
	os << ".param Vsupply=" << m_voltage << endl;
	os << ".param Tstart=" << m_Tstart << endl;
	os << ".param Tmeasure=" << m_Tmeasure << endl;
	os << ".param Tdelay=" << m_Tdelay << endl;
	os << ".param Trf=" << m_Trf << endl;
	os << ".param Tperiod='Tdelay+Tmeasure+Trf'" << endl;
	os << endl;
}
void spiceGenerator::writeSimulationSetup(ostream &os){
	writeLine(os, "simulation setup");
	os << ".dc x 0 0 1 sweep data=pat" << endl;
	os << endl;
}
void spiceGenerator::writeInputPattern(ostream &os){
	writeLine(os, "input pattern");
	//.param level_A=0
	for(UDT_STR_VEC::iterator strIt = m_pattern.m_inputPin.begin();
	strIt != m_pattern.m_inputPin.end(); ++strIt){
		os << ".param level_" << *strIt << "=0" << endl;
	}
	//.data pat level_A level_B
	os << ".data pat";
	for(UDT_STR_VEC::iterator strIt = m_pattern.m_inputPin.begin();
	strIt != m_pattern.m_inputPin.end(); ++strIt){
		os << " level_" << *strIt;
	}
	os << endl;
	//+1 0
	for(UDT_STR_VEC::iterator strIt = m_pattern.m_truthTable.begin();
	strIt != m_pattern.m_truthTable.end(); ++strIt){
		os << "+";
		for(int i=0; i<m_pattern.m_inputPin.size(); ++i){
			os << (*strIt)[i] << " ";
		}
		os << endl;
	}
	os << ".enddata" << endl;
	os << endl;
	//--------------------
	//For measuring source current
	//Rsource source_ground 0 0
	os << "Rsource 0 source_ground 0" << endl;
	//--------------------
	//Vin_A port_A source_ground DC='level_A*Vsupply'
	//Rport_A port_A A 50
	for(UDT_STR_VEC::iterator strIt = m_pattern.m_inputPin.begin();
	strIt != m_pattern.m_inputPin.end(); ++strIt){
		os << "Vin_" << *strIt << " port_" << *strIt << " source_ground DC='level_" 
			<< *strIt << "*Vsupply'" << endl;
		os << "Rport_" << *strIt << " port_" << *strIt << " " << *strIt 
			<< " " << m_Rinput << endl;
	}
	os << endl;
}
void spiceGenerator::writeOutputMeasurement(ostream &os){
	writeLine(os, "output measurement");
	//Cload_Z Z GND 10f
	//.meas dc level_Z AVG V(Z)
	for(UDT_STR_VEC::iterator strIt = m_pattern.m_outputPin.begin();
	strIt != m_pattern.m_outputPin.end(); ++strIt){
		double cload;
		if(m_useVariableCload == true && m_cell.m_drivingStrength > 0){
			cload = m_Cload*m_cell.m_drivingStrength;
		}
		else{
			cload = m_Cload;
		}
		os << "Cload_" << *strIt << " " << *strIt << " GND " << cload << "f" << endl;
		os << ".meas dc level_" << *strIt << " AVG V(" << *strIt << ")" << endl;
	}
	os << ".meas dc current_power AVG I(Rpower)" << endl;
	os << ".meas dc current_source AVG I(Rsource)" << endl;
	os << endl;
}

