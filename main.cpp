const char g_version[] = 
//
//	main.cpp
//	Defect1tfSpiceGenerator
//
//	Created by PCCO	@2015/05/29
"1.2.0"//support any statement(.ic) in subckt
;
#include "struct.h"

int main(int argc, char **argv){
	ofstream ofs;
	ifstream ifs;
	string path;
	string filename;
	spiceGenerator spiceGeneratorInst;

	if(argc != 6){
		cerr << "Version: " << g_version << endl << endl;
		cerr << "Usage ./defect1tfSpiceGenerator "
			 << "[working directory(./)] [cell name] [fault name] [value] [config file]" << endl;
		cerr << "Structure of the working directory:" << endl;
		cerr << "./[fault]/info" << endl;
		cerr << "./[fault]/[value]" << endl;
		cerr << "./info/" << endl;
		cerr << "Files read from the working directory:" << endl;
		cerr << "./info/[cell].libps (cell info file)" << endl;
		cerr << "./info/[cell].1tfpat (1tf pattern file)" << endl;
		cerr << "./info/[cell].2tfpat (2tf pattern file)" << endl;
		cerr << "./info/[cell].spips (spice netlist file)" << endl;
		cerr << "./[fault]/info/[cell].1tfdefect (defect file)" << endl;
		cerr << "Files write to the working directory:" << endl;
		cerr << "./[fault]/[value]/[cell].1tf.*.spi (simulation spice)" << endl;
		exit(-1);
	}
	path = argv[1];
	path += '/';
	spiceGeneratorInst.m_cellName = argv[2];
	spiceGeneratorInst.m_faultName = argv[3];
	spiceGeneratorInst.m_valueName = argv[4];
	cout << ">> Read config." << endl;
	filename = argv[5];
	openFile(ifs, filename);
	spiceGeneratorInst.setConfig(ifs);
	ifs.close();
	cout << ">> Read library infomation." << endl;
	filename = path + "info/" + spiceGeneratorInst.m_cellName + ".libps";
	openFile(ifs, filename);
	spiceGeneratorInst.m_cell.readSerialInfoFile(ifs);
	ifs.close();
	cout << ">> Read 1 time frame pattern." << endl;
	filename = path + "info/" + spiceGeneratorInst.m_cellName + ".1tfpat";
	openFile(ifs, filename);
	spiceGeneratorInst.m_pattern.readSerial1tfFile(ifs);
	ifs.close();
	cout << ">> Read 2 time frame pattern." << endl;
	filename = path + "info/" + spiceGeneratorInst.m_cellName + ".2tfpat";
	openFile(ifs, filename);
	spiceGeneratorInst.m_pattern.readSerial2tfFile(ifs);
	ifs.close();
	cout << ">> Read cell spice netlist." << endl;
	filename = path + "info/" + spiceGeneratorInst.m_cellName + ".spips";
	openFile(ifs, filename);
	spiceGeneratorInst.m_subckt.readSerialFile(ifs);
	ifs.close();
	cout << ">> Read defect file." << endl;
	filename = path + spiceGeneratorInst.m_faultName + "/info/" + 
		spiceGeneratorInst.m_cellName + ".1tfdefect";
	openFile(ifs, filename);
	spiceGeneratorInst.m_fault.readSerialFile(ifs);
	ifs.close();
	cout << ">> Write simulation spice." << endl;
	filename = path + spiceGeneratorInst.m_faultName + "/" + 
		spiceGeneratorInst.m_valueName + "/" + spiceGeneratorInst.m_cellName
		+ ".1tf.comb.spi";
	openFile(ofs, filename);
	spiceGeneratorInst.writeCombSpice(ofs);
	ofs.close();

	
    return 0;
} 
